



package com.packt.webstore.domain;



public class Customer {
	private String customerId;
	private String name;
	private String address;
	private String noOfOrdersMada;
	
	public Customer(String customerId, String name, String address, String noOfOrdersMada) {
		this.customerId = customerId;
		this.name = name;
		this.address = address;
		this.noOfOrdersMada = noOfOrdersMada;
	}
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getNoOfOrdersMada() {
		return noOfOrdersMada;
	}
	public void setNoOfOrdersMada(String noOfOrdersMada) {
		this.noOfOrdersMada = noOfOrdersMada;
	}
	
}
