package com.packt.webstore.domain.repository;

import java.util.*;

import com.packt.webstore.domain.Customer;


public interface CustomerRepository {
	List <Customer> getAllCustomers();
}
