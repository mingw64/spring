package com.packt.webstore.domain.repository;


import java.util.List;
import java.util.Map;
import java.util.Set;

import com.packt.webstore.domain.*;

public interface ProductRepository {
	
	List <Product> getAllProducts();
	List <Product> getProductsByCategory(String category);
	Set <Product> getProductsByFilter(Map<String, List<String>> filterParams);
	List <Product> getProductsByManufacturer(String manufacturer);
	List <Product> getProductsByPrice(String unitPrice);
	Product getProductById(String productId);
	void addProduct(Product product);
}
