package com.packt.webstore.domain.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.packt.webstore.domain.Customer;
import com.packt.webstore.domain.repository.CustomerRepository;
@Repository
public class InMemoryCustomersRepository implements CustomerRepository{

	 private List <Customer> customerList = new ArrayList<Customer>();
	
	 public InMemoryCustomersRepository() {
	 
	 Customer customer = new Customer("P123", "Pawel", "Adresowa", "123");
	 Customer customer2 = new Customer("P123", "Pawel", "Adresowa", "123");
	 
	
	 customerList.add(customer);
	 customerList.add(customer2);
	
	 }
	 
	 public List <Customer> getAllCustomers() {
		 return customerList;
		 }
}
