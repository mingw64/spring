package com.packt.webstore.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.packt.webstore.domain.*;

public interface ProductService {
	List<Product> getAllProducts();
	Product getProductById(String productID);
	List <Product> getProductsByCategory(String category);
	Set <Product> getProductsByFilter(Map<String, List<String>> filterParams);
	List <Product> getProductsByManufacturer(String manufacturer);
	List <Product> getProductsByPrice(String unitPrice);
	void addProduct(Product product);
}
