package com.packt.webstore.controller;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;



//import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.MatrixVariable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.packt.webstore.domain.Product;

//import com.packt.webstore.domain.Product;

import com.packt.webstore.service.ProductService;


@Controller
@RequestMapping("/products")
public class ProductController {
	
	@Autowired
	private ProductService productService;
     	
	/*@RequestMapping("/products")
	public String list(Model model) {
		Product iphone = new Product("P1234", "iPhone 5s", new BigDecimal(500));
		iphone.setDescription("Apple iPhone 5s, smartfon z 4-calowym wy�wietlaczem o rozdzielczo�ci 640x1136 oraz 8 megapikselowym aparatem");
		iphone.setCategory("Smart Phone");
		iphone.setManufacter("Apple");
		iphone.setUnitsInStock(1000);
		model.addAttribute("product", iphone);

		return "products";
	}*/
		
	
	@RequestMapping("/")
	public String list(Model model) {
	model.addAttribute("products", productService.getAllProducts());
	return "allproducts";
	}
	
	@RequestMapping("/{category}")
	public String getProductsByCategory(Model model, @PathVariable("category") String productCategory) {
		model.addAttribute("products", productService.getProductsByCategory(productCategory));
		return "allproducts";
	}
	
	@RequestMapping("/filter/{ByCriteria}")
	public String getProductsByFilter(@MatrixVariable(pathVar="ByCriteria") Map <String, List<String>> filterParams, Model model) {
		model.addAttribute("products", productService.getProductsByFilter(filterParams));
		return "allproducts";
	}
	
	@RequestMapping("/product")
	public String getProductById(@RequestParam("id") String productId, Model model) {
		model.addAttribute("product", productService.getProductById(productId));
		return "product";
	}
	
	
	@RequestMapping("/price/{price}")
	public String getProductsByPrice(Model model, @PathVariable("price") String productManufacturer) {
		model.addAttribute("products", productService.getProductsByPrice(productManufacturer));
		return "allproducts";
	}
	
	@RequestMapping(value= "/add", method = RequestMethod.GET)
	public String getAddNewProductForm(Model model) {
		Product newProduct = new Product();
		model.addAttribute("newProduct", newProduct);
		return "addProduct";
	}

	@RequestMapping(value="/add", method = RequestMethod.POST)
	public String processAddNewProductForm(@ModelAttribute("newProduct") Product newProduct, BindingResult result) {
		String [] supressedFields = result.getSuppressedFields();
		if(supressedFields.length>0) {
			throw new RuntimeException("Pr�ba wi�zania niedowzolonych p�l " + StringUtils.arrayToCommaDelimitedString(supressedFields));
		}
		productService.addProduct(newProduct);
		return "redirect:/products/";
	}
	
	@InitBinder
	public void initialiseBinder(WebDataBinder binder) {
		DateFormat dateFormat = new SimpleDateFormat("MM d, YYYY");
		CustomDateEditor orderDateEditor = new CustomDateEditor(dateFormat, true);
		binder.registerCustomEditor(Date.class, orderDateEditor);
		binder.setDisallowedFields("unitsInOrder", "discontinued");
	}
	
}
